# DKM-Exoplanets

## Web semantic projet:
    - construction of RDF graph about exoplanets discovery
    - management of [SPARQL](https://fr.wikipedia.org/wiki/SPARQL) database via [Jena](https://jena.apache.org/documentation/query/)
    - interactive exploration with [Sparklis](www.irisa.fr/LIS/ferre/sparklis/)
## Usage
 - Parsing csv data and output rdf database in N-triple format
```console
foo@bar:~$ python python src/data_formating.py --csv data/exo_planets.csv --output data/exo_planets.rdf
Five fist rows:
  pl_hostname     pl_name    pl_discmethod  pl_disc  ...                             pl_facility                                pl_pelink               ref_name                                            ref_url
0      11 Com    11 Com b  Radial Velocity     2007  ...                        Xinglong Station    http://exoplanet.eu/catalog/11_com_b/        Liu et al. 2008  https://ui.adsabs.harvard.edu/abs/2008ApJ...67...
1      11 UMi    11 UMi b  Radial Velocity     2009  ...  Thueringer Landessternwarte Tautenburg    http://exoplanet.eu/catalog/11_umi_b/  Dollinger et al. 2009  https://ui.adsabs.harvard.edu/abs/2009A&A...50...
2      14 And    14 And b  Radial Velocity     2008  ...       Okayama Astrophysical Observatory    http://exoplanet.eu/catalog/14_and_b/       Sato et al. 2008  https://ui.adsabs.harvard.edu/abs/2008PASJ...6...
3      14 Her    14 Her b  Radial Velocity     2002  ...                  W. M. Keck Observatory    http://exoplanet.eu/catalog/14_her_b/     Butler et al. 2003  https://ui.adsabs.harvard.edu/abs/2003ApJ...58...
4    16 Cyg B  16 Cyg B b  Radial Velocity     1996  ...                  Multiple Observatories  http://exoplanet.eu/catalog/16_cyg_b_b/    Cochran et al. 1997  https://ui.adsabs.harvard.edu/abs/1997ApJ...48...

[5 rows x 9 columns]
```
- Fuseki server
From *fuseki-server* directory
```console
foo@bar:~$ fuseki-server
```
