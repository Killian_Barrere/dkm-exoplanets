import pandas as pd
import argparse
import math
import rdflib as rdf
from rdflib.namespace import XSD, RDFS, RDF
# Common parameters for URIs
default_url = 'http://dkm-exoplanets/'
default_properties = default_url + 'property/'
default_exoplanets = default_url + 'exoplanets/'
default_hosts = default_url + 'hosts/'
default_papers = default_url + 'papers/'
default_labs = default_url + 'laboratories/'
default_methods = default_url + 'methods/'
default_type = default_url + 'type/'

def extract_data(csv_path:str) -> pd.DataFrame:
    df = pd.read_csv(csv_path, skiprows=13)
    extract_df = df["pl_disc_reflink"].str.extract(
        "<a refstr=.* href=(?P<ref_url>.*) target=ref>(?P<ref_name>.*)<\/a>")
    df["ref_name"] = extract_df["ref_name"].str.strip()
    df["ref_url"] = extract_df["ref_url"]
    return df

def create_rdf_graph(df:pd.DataFrame):
    g = rdf.Graph()

    # Create Namespaces
    dn_exoplanets = rdf.Namespace(default_exoplanets)
    dn_hosts = rdf.Namespace(default_hosts)
    dn_papers = rdf.Namespace(default_papers)
    dn_labs = rdf.Namespace(default_labs)
    dn_methods = rdf.Namespace(default_methods)
    dn_props = rdf.Namespace(default_properties)
    dn_type = rdf.Namespace(default_type)

    # Common Namespace
    DBO = rdf.Namespace("http://dbpedia.org/ontology/")
    DBP = rdf.Namespace("http://dbpedia.org/pages/")

    # Properties URIs
    hasHost = dn_props['hasHost']
    url = dn_props['url']
    discoveredBy = dn_props['dicoveredBy']
    date = DBO['date']
    name = DBO['Name']
    writtenBy = dn_props['writtenBy']
    methodUsed = dn_props['methodUsed']

    # Common classes
    c_host = dn_type['Host']
    c_lab = dn_type['Laboratory']
    c_exoplanet = dn_type['Exoplanet']
    c_method = dn_type['DiscoveryMethod']
    c_paper = dn_type['Paper']

    # Write Header of the file

    # Host
    #~ g.add(c_host, RDF.type, DBP.Star)
    g.add( (c_host, RDF.type, RDFS.Class) )
    g.add( (c_host, RDFS.subClassOf, DBP.Star)) # SubClass or Equal ?
    dn_type_url = rdf.Literal(default_type)
    g.add( (c_host, RDFS.isDefinedBy, dn_type_url))
    label_host = rdf.Literal("Star host")
    g.add( (c_host, RDFS.label, label_host)) 
    comment_host = rdf.Literal("Star on which exoplanet gravitates")
    g.add( (c_host, RDFS.comment, comment_host) )

    # Laboratory
    g.add( (c_lab, RDF.type, RDFS.Class) )
    g.add( (c_lab, RDFS.subClassOf, DBP.Organization) )
    g.add( (c_lab, RDFS.isDefinedBy, dn_type_url))
    label_lab = rdf.Literal("Laboratory")
    g.add( (c_lab, RDFS.label, label_lab) )

    # Exoplanet
    g.add( (c_exoplanet, RDF.type, RDFS.Class) )
    g.add( (c_exoplanet, RDFS.subClassOf, DBP.Exoplanet) ) # SubClass or Equal ?
    g.add( (c_exoplanet, RDFS.isDefinedBy, dn_type_url))
    label_exoplanet = rdf.Literal("Exoplanet")
    g.add( (c_exoplanet, RDFS.label, label_exoplanet) )
    comment_exoplanet = rdf.Literal("Extra solar sytem planet")
    g.add( (c_exoplanet, RDFS.comment, comment_exoplanet) )

    # Discovery Method
    g.add( (c_method, RDF.type, RDFS.Class) )
    g.add( (c_method, RDFS.subClassOf, DBP.Software) )
    g.add( (c_method, RDFS.isDefinedBy, dn_type_url))

    # Paper
    g.add( (c_paper, RDF.type, RDFS.Class) )
    g.add( (c_method, RDFS.isDefinedBy, dn_type_url))

    # hasHost
    g.add( (hasHost, RDF.type, RDF.Property) )
    g.add( (hasHost, RDFS.domain, c_exoplanet) )
    g.add( (hasHost, RDFS.range, c_host) )

    # discoveredBy
    g.add( (discoveredBy, RDF.type, RDF.Property) )
    g.add( (discoveredBy, RDFS.domain, c_exoplanet) )
    g.add( (discoveredBy, RDFS.range, c_paper) )

    # url
    g.add( (url, RDF.type, RDF.Property) )
    g.add( (url, RDFS.domain, c_exoplanet) )
    g.add( (url, RDFS.range, RDF.XMLLiteral) )
    
    # methodUsed
    g.add( (methodUsed, RDF.type, RDF.Property) )
    g.add( (methodUsed, RDFS.domain, c_paper) )
    g.add( (methodUsed, RDFS.range, c_method) )
    
    # methodUsed
    g.add( (writtenBy, RDF.type, RDF.Property) )
    g.add( (writtenBy, RDFS.domain, c_paper) )
    g.add( (writtenBy, RDFS.range, c_lab) )
    
    
    for i in range(len(df)):
        # =========================
        # Exoplanet characteristics
        # =========================
        p = df.iloc[i]

        # Ignore invalid exoplanet and reference url
        if not isinstance(p['url'], str) or not isinstance(p['ref_url'], str):
            continue
        # Exoplanet URI
        exoplanet = rdf.URIRef(p['url'][:-1] + '#uri')

        # Exoplanet URL
        exoplanet_url = rdf.Literal(p['url'])

        #Exoplanet Name
        exoplanet_name = rdf.Literal(p['exoplanet_name'])

        # Host URI
        host = dn_hosts[p['host_name'].replace(' ', '_')]

        host_name = rdf.Literal(p['host_name'])
        # Paper URI
        paper = rdf.URIRef(p['ref_url'].replace(' ', '') + '#uri')

        # Paper URL
        paper_url = rdf.Literal(p['ref_url'].replace(' ', ''))

        # Year of discovery
        year = rdf.Literal(p['date'], datatype=XSD.gYear)

        # Laboratory
        laboratory = rdf.URIRef(dn_labs[p['laboratory'].replace(' ', '_')])

        # Method
        method = rdf.URIRef(dn_methods[p['method'].replace(' ', '_')])

        # =====================
        # Write inside the file
        # =====================
        # Host name
        g.add( (host, name, host_name) )
        # Has Host
        g.add( (exoplanet, hasHost, host) )

        # URL
        g.add( (exoplanet, url, exoplanet_url) )

        # Name
        g.add( (exoplanet, name, exoplanet_name) )

        # Discovered by
        g.add( (exoplanet, discoveredBy, paper) )

        # Paper url
        g.add( (paper, url, paper_url) )

        # Year of discovery
        g.add( (paper, date, year) )

        # Laboratory
        g.add( (paper, writtenBy, laboratory) )

        # Method used
        g.add( (paper, methodUsed, method) )

    return g

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--csv", type=str, required=True, help="csv path")
    parser.add_argument("--output", type=str, required=True, help="Path to the outputed RDF file")
    parser.add_argument("--format", type=str, choices=['xml', 'n3', 'turtle', 'nt', 'pretty-xml', 'trix', 'trig', 'nquads'], default='xml', help="Format of the outputed RDF file")
    args = parser.parse_args()

    print("Reading the CSV file...")
    df = extract_data(args.csv)

    # Renaming the columns for clarity purposes
    rename_dict = {'pl_hostname' : 'host_name',
                   'pl_name' : 'exoplanet_name',
                   'pl_discmethod' : 'method',
                   'pl_disc' : 'date',
                   #'pl_disc_reflink',
                   'pl_facility' : 'laboratory',
                   'pl_pelink'  : 'url'
                   #'ref_name',
                   #'ref_url'
                   }

    df.rename(columns=rename_dict, inplace=True)

    print("Five first rows:")
    print("")
    print(df.head())


    # Head and RDFS Properties ?

    # Write the core of the RDF file
    print("")
    print("Writing the RDF file...")

    g = create_rdf_graph(df)

    # Save the graph in the file
    # 'xml', 'n3', 'turtle', 'nt', 'pretty-xml', 'trix', 'trig' and 'nquads' are built in.

    g.serialize(destination=args.output, format=args.format)
